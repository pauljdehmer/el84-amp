EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L power:GND #PWR03
U 1 1 6166A739
P 5200 2400
F 0 "#PWR03" H 5200 2150 50  0001 C CNN
F 1 "GND" H 5205 2227 50  0000 C CNN
F 2 "" H 5200 2400 50  0001 C CNN
F 3 "" H 5200 2400 50  0001 C CNN
	1    5200 2400
	1    0    0    -1  
$EndComp
Wire Wire Line
	4650 2000 5200 2000
Wire Wire Line
	5200 2000 5200 2100
$Comp
L Device:CP C1
U 1 1 61671F7B
P 5200 2250
F 0 "C1" H 5318 2296 50  0000 L CNN
F 1 "47uF/350V" H 5318 2205 50  0000 L CNN
F 2 "Capacitor_THT:CP_Axial_L26.5mm_D20.0mm_P33.00mm_Horizontal" H 5238 2100 50  0001 C CNN
F 3 "~" H 5200 2250 50  0001 C CNN
	1    5200 2250
	1    0    0    -1  
$EndComp
Wire Wire Line
	5200 2100 4950 2100
Connection ~ 5200 2100
$Comp
L Device:R R2
U 1 1 6167633C
P 4950 2250
F 0 "R2" H 4880 2296 50  0000 R CNN
F 1 "220K/1W" H 4880 2205 50  0000 R CNN
F 2 "Resistor_THT:R_Axial_DIN0411_L9.9mm_D3.6mm_P15.24mm_Horizontal" V 4880 2250 50  0001 C CNN
F 3 "~" H 4950 2250 50  0001 C CNN
	1    4950 2250
	1    0    0    -1  
$EndComp
Wire Wire Line
	4950 2400 5200 2400
Connection ~ 5200 2400
$Comp
L Device:R R3
U 1 1 61678212
P 5600 2000
F 0 "R3" V 5807 2000 50  0000 C CNN
F 1 "100/1W" V 5716 2000 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0411_L9.9mm_D3.6mm_P15.24mm_Horizontal" V 5530 2000 50  0001 C CNN
F 3 "~" H 5600 2000 50  0001 C CNN
	1    5600 2000
	0    -1   -1   0   
$EndComp
Wire Wire Line
	5200 2000 5450 2000
Connection ~ 5200 2000
$Comp
L power:GND #PWR05
U 1 1 6167F6EC
P 5900 2650
F 0 "#PWR05" H 5900 2400 50  0001 C CNN
F 1 "GND" H 5905 2477 50  0000 C CNN
F 2 "" H 5900 2650 50  0001 C CNN
F 3 "" H 5900 2650 50  0001 C CNN
	1    5900 2650
	1    0    0    -1  
$EndComp
Wire Wire Line
	5900 2000 5900 2100
$Comp
L Device:CP C3
U 1 1 6167F6F3
P 5900 2250
F 0 "C3" H 6018 2296 50  0000 L CNN
F 1 "47uF/350V" H 6018 2205 50  0000 L CNN
F 2 "Capacitor_THT:CP_Axial_L26.5mm_D20.0mm_P33.00mm_Horizontal" H 5938 2100 50  0001 C CNN
F 3 "~" H 5900 2250 50  0001 C CNN
	1    5900 2250
	1    0    0    -1  
$EndComp
Wire Wire Line
	5750 2000 5900 2000
Wire Wire Line
	5900 2000 6100 2000
Connection ~ 5900 2000
Wire Wire Line
	5900 2000 5900 1450
Text GLabel 5900 1450 0    50   Input ~ 0
OT-Brown
$Comp
L power:GND #PWR07
U 1 1 6168C4E4
P 6600 2400
F 0 "#PWR07" H 6600 2150 50  0001 C CNN
F 1 "GND" H 6605 2227 50  0000 C CNN
F 2 "" H 6600 2400 50  0001 C CNN
F 3 "" H 6600 2400 50  0001 C CNN
	1    6600 2400
	1    0    0    -1  
$EndComp
Wire Wire Line
	6600 2000 6600 2100
$Comp
L Device:CP C4
U 1 1 6168C5BF
P 6600 2250
F 0 "C4" H 6718 2296 50  0000 L CNN
F 1 "47uF/350V" H 6718 2205 50  0000 L CNN
F 2 "Capacitor_THT:CP_Axial_L26.5mm_D20.0mm_P33.00mm_Horizontal" H 6638 2100 50  0001 C CNN
F 3 "~" H 6600 2250 50  0001 C CNN
	1    6600 2250
	1    0    0    -1  
$EndComp
Wire Wire Line
	6400 2000 6600 2000
$Comp
L Valve:EL84 U1
U 1 1 6169CEC9
P 6200 3600
F 0 "U1" H 6200 4231 50  0000 C CNN
F 1 "EL84" H 6200 4140 50  0000 C CNN
F 2 "Valve:Valve_Noval_P" H 6500 3200 50  0001 C CNN
F 3 "http://www.r-type.org/pdfs/el84.pdf" H 6200 3600 50  0001 C CNN
	1    6200 3600
	1    0    0    -1  
$EndComp
$Comp
L Device:R_POT RV1
U 1 1 616A9791
P 5450 3650
F 0 "RV1" H 5381 3696 50  0000 R CNN
F 1 "1MEG LOG" H 5381 3605 50  0000 R CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x03_P2.54mm_Vertical" H 5450 3650 50  0001 C CNN
F 3 "~" H 5450 3650 50  0001 C CNN
	1    5450 3650
	1    0    0    -1  
$EndComp
$Comp
L Device:R R4
U 1 1 616B106C
P 5750 3650
F 0 "R4" V 5957 3650 50  0000 C CNN
F 1 "5.6K" V 5866 3650 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" V 5680 3650 50  0001 C CNN
F 3 "~" H 5750 3650 50  0001 C CNN
	1    5750 3650
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR04
U 1 1 616BBC3F
P 5450 3800
F 0 "#PWR04" H 5450 3550 50  0001 C CNN
F 1 "GND" H 5455 3627 50  0000 C CNN
F 2 "" H 5450 3800 50  0001 C CNN
F 3 "" H 5450 3800 50  0001 C CNN
	1    5450 3800
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR06
U 1 1 616C7D5B
P 6100 4900
F 0 "#PWR06" H 6100 4650 50  0001 C CNN
F 1 "GND" H 6105 4727 50  0000 C CNN
F 2 "" H 6100 4900 50  0001 C CNN
F 3 "" H 6100 4900 50  0001 C CNN
	1    6100 4900
	1    0    0    -1  
$EndComp
$Comp
L Device:R R5
U 1 1 616CBE16
P 6100 4750
F 0 "R5" H 6170 4796 50  0000 L CNN
F 1 "130/1W" H 6170 4705 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0411_L9.9mm_D3.6mm_P15.24mm_Horizontal" V 6030 4750 50  0001 C CNN
F 3 "~" H 6100 4750 50  0001 C CNN
	1    6100 4750
	1    0    0    -1  
$EndComp
$Comp
L Device:CP C2
U 1 1 616CDAB3
P 5850 4750
F 0 "C2" H 5733 4796 50  0000 R CNN
F 1 "100uF/25V" H 5733 4705 50  0000 R CNN
F 2 "Capacitor_THT:CP_Axial_L11.0mm_D8.0mm_P15.00mm_Horizontal" H 5888 4600 50  0001 C CNN
F 3 "~" H 5850 4750 50  0001 C CNN
	1    5850 4750
	1    0    0    -1  
$EndComp
Wire Wire Line
	5850 4600 6100 4600
Wire Wire Line
	6100 3950 6100 4600
Connection ~ 6100 4600
Wire Wire Line
	5850 4900 6100 4900
Connection ~ 6100 4900
Wire Wire Line
	6600 2000 7100 2000
Wire Wire Line
	7100 2000 7100 3550
Connection ~ 6600 2000
$Comp
L Device:R R7
U 1 1 616DF6EA
P 6650 3550
F 0 "R7" V 6857 3550 50  0000 C CNN
F 1 "1K/1W" V 6766 3550 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0411_L9.9mm_D3.6mm_P15.24mm_Horizontal" V 6580 3550 50  0001 C CNN
F 3 "~" H 6650 3550 50  0001 C CNN
	1    6650 3550
	0    -1   -1   0   
$EndComp
Wire Wire Line
	7100 3550 6800 3550
Text GLabel 6400 3150 2    50   Input ~ 0
OT-Blue
Wire Wire Line
	6200 3150 6400 3150
Text GLabel 2550 2350 0    50   Input ~ 0
PT-Red(1)
Text GLabel 2550 2650 0    50   Input ~ 0
PT-Red(2)
$Comp
L Device:R R6
U 1 1 61682A0F
P 6250 2000
F 0 "R6" V 6457 2000 50  0000 C CNN
F 1 "1K/1W" V 6366 2000 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0411_L9.9mm_D3.6mm_P15.24mm_Horizontal" V 6180 2000 50  0001 C CNN
F 3 "~" H 6250 2000 50  0001 C CNN
	1    6250 2000
	0    -1   -1   0   
$EndComp
$Comp
L Connector:Conn_01x03_Female J3
U 1 1 616A58C6
P 850 1600
F 0 "J3" H 742 1275 50  0000 C CNN
F 1 "Mains" H 742 1366 50  0000 C CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x03_P2.54mm_Vertical" H 850 1600 50  0001 C CNN
F 3 "~" H 850 1600 50  0001 C CNN
	1    850  1600
	-1   0    0    1   
$EndComp
$Comp
L Connector:Conn_01x07_Female J1
U 1 1 616A1CB1
P 850 2500
F 0 "J1" H 742 1975 50  0000 C CNN
F 1 "Hammond-269EX" H 742 2066 50  0000 C CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x07_P2.54mm_Vertical" H 850 2500 50  0001 C CNN
F 3 "~" H 850 2500 50  0001 C CNN
	1    850  2500
	-1   0    0    1   
$EndComp
$Comp
L power:GND #PWR0101
U 1 1 616DCD8C
P 1050 1500
F 0 "#PWR0101" H 1050 1250 50  0001 C CNN
F 1 "GND" H 1055 1327 50  0000 C CNN
F 2 "" H 1050 1500 50  0001 C CNN
F 3 "" H 1050 1500 50  0001 C CNN
	1    1050 1500
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR0102
U 1 1 616ED619
P 1050 2500
F 0 "#PWR0102" H 1050 2250 50  0001 C CNN
F 1 "GND" H 1055 2327 50  0000 C CNN
F 2 "" H 1050 2500 50  0001 C CNN
F 3 "" H 1050 2500 50  0001 C CNN
	1    1050 2500
	0    -1   -1   0   
$EndComp
Text GLabel 1050 2200 2    50   Input ~ 0
PT-Green(2)
Text GLabel 1050 2300 2    50   Input ~ 0
PT-Green(1)
Text GLabel 1050 2400 2    50   Input ~ 0
PT-Red(2)
Text GLabel 1050 2600 2    50   Input ~ 0
PT-Red(1)
Text GLabel 1050 2800 2    50   Input ~ 0
PT-White
Text GLabel 1050 2700 2    50   Input ~ 0
PT-Gray
Text GLabel 1050 1600 2    50   Input ~ 0
Mains-Neutral
Text GLabel 1050 1700 2    50   Input ~ 0
Mains-Line
Text GLabel 2600 1600 0    50   Input ~ 0
Mains-Neutral
Text GLabel 2800 1700 2    50   Input ~ 0
PT-White
Wire Wire Line
	2850 2350 2850 2500
Text GLabel 3050 2500 2    50   Input ~ 0
HV
Wire Wire Line
	3050 2500 2850 2500
Connection ~ 2850 2500
Wire Wire Line
	2850 2500 2850 2650
Text GLabel 5850 4600 0    50   Input ~ 0
PT-Green(1)
Text GLabel 5450 3300 0    50   Input ~ 0
Audio-Input
$Comp
L power:GND #PWR02
U 1 1 616C2DC6
P 1050 950
F 0 "#PWR02" H 1050 700 50  0001 C CNN
F 1 "GND" V 1055 822 50  0000 R CNN
F 2 "" H 1050 950 50  0001 C CNN
F 3 "" H 1050 950 50  0001 C CNN
	1    1050 950 
	0    -1   -1   0   
$EndComp
Text GLabel 1050 1050 2    50   Input ~ 0
Autio-Input
$Comp
L Connector:Conn_01x02_Female J4
U 1 1 6176803A
P 850 1050
F 0 "J4" H 742 725 50  0000 C CNN
F 1 "Audio Input" H 742 816 50  0000 C CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x02_P2.54mm_Vertical" H 850 1050 50  0001 C CNN
F 3 "~" H 850 1050 50  0001 C CNN
	1    850  1050
	-1   0    0    1   
$EndComp
$Comp
L Connector:Conn_01x06_Female J6
U 1 1 6180FE3C
P 850 3600
F 0 "J6" H 742 3075 50  0000 C CNN
F 1 "Hammond-125DSE" H 742 3166 50  0000 C CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x06_P2.54mm_Vertical" H 850 3600 50  0001 C CNN
F 3 "~" H 850 3600 50  0001 C CNN
	1    850  3600
	-1   0    0    1   
$EndComp
Text GLabel 1050 3800 2    50   Input ~ 0
OT-Brown
Text GLabel 1050 3700 2    50   Input ~ 0
OT-Blue
Text GLabel 1050 3600 2    50   Input ~ 0
OT-Black
Text GLabel 1050 3500 2    50   Input ~ 0
OT-Green
Text GLabel 1050 3400 2    50   Input ~ 0
OT-Yellow
Text GLabel 1050 3300 2    50   Input ~ 0
OT-White
$Comp
L Connector:Conn_01x06_Female J7
U 1 1 6181700E
P 850 4550
F 0 "J7" H 742 4025 50  0000 C CNN
F 1 "Speaker Output" H 742 4116 50  0000 C CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x06_P2.54mm_Vertical" H 850 4550 50  0001 C CNN
F 3 "~" H 850 4550 50  0001 C CNN
	1    850  4550
	-1   0    0    1   
$EndComp
Text GLabel 1050 4550 2    50   Input ~ 0
OT-Yellow
Text GLabel 1050 4350 2    50   Input ~ 0
OT-White
Text GLabel 1050 4750 2    50   Input ~ 0
OT-Green
Text GLabel 1050 4250 2    50   Input ~ 0
OT-Black
Text GLabel 1050 4450 2    50   Input ~ 0
OT-Black
Text GLabel 1050 4650 2    50   Input ~ 0
OT-Black
Text GLabel 2700 3550 2    50   Input ~ 0
OT-Black
Text GLabel 5800 2550 0    50   Input ~ 0
OT-Black
Text GLabel 2400 3550 0    50   Input ~ 0
OT-White
$Comp
L Device:R R1
U 1 1 6184C276
P 2550 3550
F 0 "R1" V 2757 3550 50  0000 C CNN
F 1 "220/5W" V 2666 3550 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0614_L14.3mm_D5.7mm_P20.32mm_Horizontal" V 2480 3550 50  0001 C CNN
F 3 "~" H 2550 3550 50  0001 C CNN
	1    2550 3550
	0    -1   -1   0   
$EndComp
Text GLabel 2600 1700 0    50   Input ~ 0
Mains-Line
Wire Wire Line
	5900 2400 5900 2550
Wire Wire Line
	5800 2550 5900 2550
Connection ~ 5900 2550
Wire Wire Line
	5900 2550 5900 2650
Wire Wire Line
	5450 3300 5450 3500
$Comp
L Device:D D1
U 1 1 616B49A8
P 2700 2350
F 0 "D1" H 2700 2133 50  0000 C CNN
F 1 "UF4007" H 2700 2224 50  0000 C CNN
F 2 "Diode_THT:D_5W_P12.70mm_Horizontal" H 2700 2350 50  0001 C CNN
F 3 "~" H 2700 2350 50  0001 C CNN
	1    2700 2350
	-1   0    0    1   
$EndComp
$Comp
L Device:D D2
U 1 1 616B568E
P 2700 2650
F 0 "D2" H 2700 2433 50  0000 C CNN
F 1 "UF4007" H 2700 2524 50  0000 C CNN
F 2 "Diode_THT:D_5W_P12.70mm_Horizontal" H 2700 2650 50  0001 C CNN
F 3 "~" H 2700 2650 50  0001 C CNN
	1    2700 2650
	-1   0    0    1   
$EndComp
Text GLabel 4650 2000 0    50   Input ~ 0
HV
Text GLabel 2800 1600 2    50   Input ~ 0
PT-Gray
Wire Wire Line
	2600 1600 2800 1600
Wire Wire Line
	2800 1700 2600 1700
$EndSCHEMATC
